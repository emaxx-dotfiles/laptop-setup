#!/usr/bin/env bash

set -u

# SETUP VARS
DOTFILEDIR="$BASEDIR/dotfiles"

BREWFILE="$BASEDIR/brew/Brewfile"
CASKFILE="$BASEDIR/cask/Caskfile"

BREW=/usr/local/bin/brew



show_about() {
  whiptail --title "About macOStrap" --msgbox "
  \\nThis tool provides some basic configs, apps and packages to get you setup and productive quickly.
  \\n
  \\nVisit the following github repo for more information and feel free to leave feedback and file an issue in case you encounter any bugs:
  - Git Repo: $GITHUB_REPO_URL" 20 80
}

show_main_menu() {
  choice=$(whiptail --title "Welcome to macOStrap" --menu "\nSelect what you want to do" 0 0 0 --cancel-button Exit --ok-button Execute \
  "About macOStrap"    "Information about the macOStrap tool" \
  "" "" \
  "Tools" "Install ZSH & ZSH Configs, and other Custom Tools ►"\
  "Apps"      "Select apps to install via brew-cask ►" \
  "Brew Packages"      "Select brew packages ►" \
  "Components & Configs"      "Select additional components & configs to install ►" \
  "Fun stuff"     "Fun little utilities for cli, tui and gui ►" \
  "Dotfile install"    "Install dotfiles" \
  3>&1 1>&2 2>&3)
  RET=$?
  if [ $RET -eq 1 ] || [ $RET -eq 255 ]; then
    # "Exit" button selected or <Esc> key pressed two times
    echo "Exiting."
    exit 0
  fi

  if [[ "$choice" == "" ]]; then
    true

  elif [[ "$choice" == *"About"* ]]; then
    show_about

  elif [[ "$choice" == *"Tools"* ]]; then
    additionalCompsConf=$(whiptail --title "Additional Components & Configurations" --menu "\nSelect the components and configs you want to install:\n\n[enter] = install\n[tab] = switch to Buttons / List" 0 0 0 --cancel-button Back --ok-button Install \
      "zsh    "     "Install ZSH, zPlug & .zshrc" \
      "emacs   " "Install Emacs and Doom" \
      "resign"  "Install iOS App Resigner" \
      "iterm2    "    "Install iTerm2 themes & config"\
    "mac defaults    "    "Change mac defaults (e.g. show the ~/Library/ Folder)"\
    3>&1 1>&2 2>&3)
    if [ $? -eq 1 ] || [ $? -eq 255 ]; then return 0; fi
    case "$additionalCompsConf" in
      zsh\ *) zsh_install;;
      iterm2\ *) sh "$BASEDIR/mac/iterm2_install.sh";;
      emacs\ *)  emacs_install;;
      resign\ *) app_resign_install;;
      mac\ *) sh "$BASEDIR/mac/mac_install.sh";;
      "") return ;;
      *) whiptail --msgbox "A not supported option was selected (probably a programming error):\\n  \"$additionalCompsConf\"" 8 80 ;;
    esac

  elif [[ "$choice" == *"Apps"* ]]; then
    apps=$(whiptail --separate-output --title "Apps" --checklist "\nSelect the apps you want to install:\n\n[spacebar] = toggle on/off\n[tab] = switch to Buttons / List" 0 0 0 --cancel-button Back --ok-button Install \
      1password "1Password Password Manager" on \
      alfred "Alfred - a better spotlight" on \
      android-studio "Android Studio for development" off \
      android-sdk "Android SDK for development" off \
      atom "Atom Text Editor" off \
      bettertouchtool "BetterTouchTool (Window Snapping)" off \
      caffeine "Caffeine (Disable System sleep)" off \
      carthage "Carthage - a cocoapods alternative" off \
      arduino "Arduino SDK" off \
      cyberduck "Cyberduck – Serverbrowser" off \
      docker "Docker Runtime" on \
      google-chrome "Google Chrome (stable)" on \
      google-chrome-canary "Google Chrome (Canary)" off\
      firefox "Firefox (stable)" on \
      firefox-developer-edition "Firefox (Developer-Edition)" off\
      iterm2 "iTerm2 - the better terminal" on \
      intellij-idea "IntelliJ IDEA" off \
      java "Java (most recent)" off \
      java8 "Java 8" off \
      keybase "Keybase" on \
      macdown "MacDown - A MarkDown Editor" on \
      postman "Postman - API Testing Tool" on \
      redis "Redis - in-memory kv store" on \
      sequel-pro "Sequel Pro - mySQL Client" off \
      slack "Slack" on \
      spotify "Spotify" on \
      tableplus "Tableplus - a psql client" on \
      tig "tig" on \
      tunnelblick "Tunnelblick VPN" off \
      ultimaker-cura "Cura Slicer (3D-Printing)" off \
      vagrant "Vagrant" off \
      virtualbox "VirtualBox – VM Manager" on \
      virtualbox-extension-pack "VirtualBox Extensions" off \
      visual-studio-code "Visual Studio Code" on \
      vlc "VLC" on \
    3>&1 1>&2 2>&3)

    # Only write file if something is selected
    if [[ "${#apps}" > 0 ]]; then
      chkRmExistingFile $CASKFILE
      while read -r line; do
        echo -e "$line" >> $CASKFILE
      done <<< "$apps"
      brew list $(cat $CASKFILE|grep -v "#") &>/dev/null || brew install $(cat $CASKFILE|grep -v "#")
    fi

    if [ $? -eq 1 ] || [ $? -eq 255 ]; then return 0; fi

    
  elif [[ "$choice" == *"Fun"* ]]; then
  funPackages=$(whiptail --separate-output --title "Brew" --checklist "\nSelect the brew packages you want to install:\n\n[spacebar] = toggle on/off\n[tab] = switch to Buttons / List" 0 0 0 --cancel-button Back --ok-button Install \
    spotify-tui "Spotify terminal app" on \
    tuir "Reddit terminal app" on \
    youtube-dl "Youtube downloader" on \
    3>&1 1>&2 2>&3)

    # Only write file if something is selected
    if [[ "${#funPackages}" > 0 ]]; then
      chkRmExistingFile $BREWFILE
      while read -r line; do
        echo -e "$line" >> $BREWFILE
      done <<< "$brewPackages"
      brew list $(cat $BREWFILE|grep -v "#") &>/dev/null || brew install $(cat $BREWFILE|grep -v "#")
    fi

    if [ $? -eq 1 ] || [ $? -eq 255 ]; then return 0; fi


  elif [[ "$choice" == *"Brew"* ]]; then
    brewPackages=$(whiptail --separate-output --title "Brew" --checklist "\nSelect the brew packages you want to install:\n\n[spacebar] = toggle on/off\n[tab] = switch to Buttons / List" 0 0 0 --cancel-button Back --ok-button Install \
      autoconf "Automatic configure script builder" on \
      automake "Tool for generating GNU Standards-compliant Makefiles" on \
      asdf "tool version management for nodejs, python and more" on \
      bat "A cat clone with wings" on \
      betterzip "A selection of ZIP utilities for quicklook" on \
      coreutils "Core utilities" on \
      curl "Get a file from an HTTP, HTTPS or FTP server" on \
      diff-so-fancy "Better diffs" on \
      fd "find alternative" on \
      findutils "Additional Find utils" on \
      fzf "fuzzy finder" on \
      git "The complete git experience (no cut content!)" on \
      git-flow "Extensions to follow Vincent Driessen's branching model" off \
      gnu-sed "GNU sed is better then BSD sed" on \
      httpd "Apache HTTPD" on \
      kubectx "Kubectx for kubernetes contexts" on \
      mariadb "Drop-in replacement for MySQL" on \
      mas "Mac App Store command-line interface" on \
      maven "Java-based project management" off \
      moreutils "Additional UNIX utils" on \
      nvm "Manage multiple Node.js versions" off \
      openssl "SSL/TLS cryptography library" on \
      php "PHP (most recent)" off \
      phpmyadmin "Web interface for MySQL and MariaDB" off \
      python "Python 2 updates" on \
      python3 "Python 3" on \
      qlcolorcode "Syntax highlighting in quicklooks" on \
      qlimagesize "See Image size and resolution in Preview" on \
      qlmarkdown "Preview parsed MD files" on \
      qlprettypatch "Preview Patch files" on \
      qlstephen "Preview plain text files with or without a file extension" on \
      qlvideo "Preview Video, metadata and thumbnails" on \
      quicklook-csv "Preview formatted CSV files" on \
      quicklook-json "Preview JSON files formatted with syntax highlighting" on \
      sqlite "Command-line interface for SQLite" on \
      ripgrep "better grep" on \
      ssh-copy-id "Add a public key to a remote machines authorized_keys file" on \
      thefuck "autocorrect for your terminal" on \
      tldr "Better man pages" on \
      tmux "Terminal multiplexer" on \
      tree "Display directories as trees (with optional color/HTML output)" on \
      vim "Vi with many additional features" on \
      wakatime-cli "Time tracking for editors" on \
      webpquicklook "Preview webp files" on \
      wget "Internet file retriever" on \
    3>&1 1>&2 2>&3)

    # Only write file if something is selected
    if [[ "${#brewPackages}" > 0 ]]; then
      chkRmExistingFile $BREWFILE
      while read -r line; do
        echo -e "$line" >> $BREWFILE
      done <<< "$brewPackages"
      brew list $(cat $BREWFILE|grep -v "#") &>/dev/null || brew install $(cat $BREWFILE|grep -v "#")
    fi

    if [ $? -eq 1 ] || [ $? -eq 255 ]; then return 0; fi

  elif [[ "$choice" == *"Components"* ]]; then
    additionalCompsConf=$(whiptail --title "Additional Components & Configurations" --menu "\nSelect the components and configs you want to install:\n\n[enter] = install\n[tab] = switch to Buttons / List" 0 0 0 --cancel-button Back --ok-button Install \
    "rvm    "     "Manage multiple Ruby Versions" \
    3>&1 1>&2 2>&3)
    if [ $? -eq 1 ] || [ $? -eq 255 ]; then return 0; fi
    case "$additionalCompsConf" in
      rvm\ *) rvm_install;;
      "") return ;;
      *) whiptail --msgbox "A not supported option was selected (probably a programming error):\\n  \"$additionalCompsConf\"" 8 80 ;;
    esac

  elif [[ "$choice" == *"Dotfile"* ]]; then
    dotfiles_install;
  fi
}
