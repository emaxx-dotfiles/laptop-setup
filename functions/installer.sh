#!/usr/bin/env bash

homebrew_install() {
  echo "Installing homebrew..."
  if [ ! -f /usr/local/bin/brew ]; then
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
    brew tap homebrew/cask-versions
  else
    echo "Homebrew already installed. So far, so good."
  fi
}

whiptail_install() {
  echo "Installing whiptail..."
  if [ ! -f /usr/local/bin/whiptail ]; then
    brew install newt
  else
    # Test whiptail version
    whiptail -v > /dev/null 2>/dev/null
    if [[ $? -ne 0 ]]; then
      brew reinstall newt
    else
      echo "Whiptail already installed. Going on..."
    fi
  fi
}

rvm_install() {
  mkdir -p ~/.rvm/src && cd ~/.rvm/src && rm -rf ./rvm && \
  git clone --depth 1 https://github.com/rvm/rvm.git && \
  cd rvm && ./install
  source ~/.rvm/scripts/rvm
}

zsh_install() {
  # Symlink macOStraps .zshrc to your $HOME
  ln -s $BASEDIR/dotfiles/.zshrc ~/.zshrc

  # Install zsh and zplug
  brew install zsh zplug
  echo
  echo "+––––––––––––––––+"
  echo "| PLEASE NOTICE! |"
  echo "+––––––––––––––––+"
  echo
  echo "We are about to add brews zsh to your /etc/shells and activate zsh for the"
  echo "first time."
  echo "For this we need superuser privileges."
  echo

  # Add brew zsh to /etc/shells and switch default-shell
  if [ ! "grep -Fx $(echo $( which zsh )) /etc/shells" ]; then
    echo "Adding /usr/local/bin/zsh to /etc/shells"
    sudo /bin/sh -c '/usr/local/bin/zsh >> /etc/shells'
  fi

  chsh -s /usr/local/bin/zsh

  # Install fzf fuzzy completion
  brew install fzf && $(brew --prefix)/opt/fzf/install

}

emacs_install() {
  brew tap railwaycat/emacsmacport
  brew install emacs-mac --with-modules --with-mac-metal
  if [ ! -f ~/.emacs.d ]; then
    git clone https://github.com/hlissner/doom-emacs ~/.emacs.d
    ~/.emacs.d/bin/doom install
  fi
}

cocoapods_install() {
  gem install cocoapods
}

mongodb_install() {
  brew tap mongodb/brew
  brew install mongodb-community@4.4
}

nodejs_setup() {
  if ! command -v asdf &> /dev/null
then
  echo "asdf not installed or configured"
  else
    asdf plugin add nodejs
    asdf install nodejs 11.14.0
    asdf install nodejs 12.3.1
    asdf install nodejs 14.5.0
    asdf global nodejs 14.5.0
    npm i -g yarn eslint prettier
  fi
}

dotfiles_install() {
    git clone https://gitlab.com/emaxx-dotfiles/dots.git ~/.dotfiles
    ~/.dotfiles/script/bootstrap
	  ~/.dotfiles/script/installer
}

app_resign_install() {
  wget https://github.com/DanTheMan827/ios-app-signer/releases/download/1.13.1/iOS.App.Signer.app.zip -O /tmp/resign.zip
  unzip /tmp/resign.zip
  mv tmp/iOS\ App\ Signer.app /Applications
  rm -rf /tmp/resign.zip
}
