#!/usr/bin/env bash

git clone https://gitlab.com/emaxx-dotfiles/laptop-setup.git "/tmp/macOStrap$(date +%s )"
cd "/tmp/macOStrap"*

./install.sh
